console.log("connected");

var player1 = prompt("Player One: Enter Your Name, you will be Blue");
var player1Color = "rgb(0, 0, 255)";

var player2 = prompt("Player Two: Enter your Name, you will be Red");
var player2Color = "rgb(255, 0, 0)";

var color = player1Color;

function startGame(){
  //Locate the column that the user clicked
  var col = $(this).closest("td").index();
  //Identify if a chip has been dropped already
  //Find the level where the next chip needs to drop.
  var rowIndex = getLastChip(col);
  console.log( "row = " + rowIndex + " column " + col);
  //drop the chip and change the board color
  changeChipColor(rowIndex,col, color);
  //check if someone won
  if (horizontalWinCheck(rowIndex, color) || verticalWinCheck(col, color) || diagonalWinCheck(color)){
    gameOver();
  }
  //If they won - game is over
  //else go to next turn
  else if (color == player1Color){
    color = player2Color;
  }
  else{
    color = player1Color
  }
}
function gameOver(){
  alert("Congratulations! You win!");
}
function getColor(rowIndex, colIndex){
    return $("table tr").eq(rowIndex).find("td").eq(colIndex).find("button").css("background-color");
}

function getLastChip(colIndex){
    var buttonColor = 0;
    for (var i = 5; i>-1; i--){
      buttonColor = getColor(i, colIndex);
        if (buttonColor == "rgb(128, 128, 128)"){
        return i;
      }
    }
}

function changeChipColor(rowIndex, colIndex, color){
    $("table tr").eq(rowIndex).find("td").eq(colIndex).find("button").css("background-color", color);
}

function horizontalWinCheck(rowIndex, color){
  var chipColor;
  var connectFour = 0;
  for(var col = 0; col<8; col++){
    chipColor = getColor(rowIndex, col);
    if(chipColor == color){
      connectFour++;
      if(connectFour == 4){
        console.log("Horizontals")
        return true;
      }
    }
    else{
      connectFour = 0;
      if (col>=5){
        return false;
      }
    }
  }
return false;
}

function verticalWinCheck(colIndex, color){
  var chipColor;
  var connectFour=0;
  for(var row=0; row<6; row++){
    chipColor = getColor(row, colIndex);
    if(chipColor == color){
      connectFour++;
      if(connectFour == 4){
        console.log("Vericals")
        return true;
      }
    }
    else{
      connectFour = 0;
      if (row>=3){
        return false;
      }
    }
}
}

function matchCheck(one, two, three, four, color){
  console.log(one + " " + two + " " + three + " " + four);
  return (one===two && two===three && three===four && four===color);
}
function diagonalWinCheck(color) {
  for (var row=0; row<6; row++){
    for(var col=0; col<4; col++){
      if(row<4){
        if (matchCheck(getColor(row,col), getColor(row+1, col+1), getColor(row+2, col+2), getColor(row+3, col+3), color)){
          return true;
        }
      }
      else{
        if (matchCheck(getColor(row,col), getColor(row-1, col+1), getColor(row-2, col+2), getColor(row-3, col+3), color)){
          return true;
        }
      }
    }
  }
}

$("h3").text("Player one it's your turn, make your move!");

$(".board button").on("click", startGame);
